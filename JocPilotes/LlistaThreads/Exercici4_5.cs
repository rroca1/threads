﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;

namespace LlistaThreads
{
    public class Exercici4_5 : Exercici
    {
        int nPunts;
        int contador;
        private readonly object locker = new object();
        public Exercici4_5()
        {
            nPunts = 100000;
            contador = 0;

        }
        public void RunSenseThreads()
        {
            contador = 0;
            Random r = new Random();
            DateTime tempsInici = DateTime.Now;
            CalculaPi(r, 0, nPunts);
            double resultat = 4.0 * (contador / (double)nPunts);
            Console.WriteLine("Resultat " + resultat);
            Console.WriteLine("Ha tardat " + (DateTime.Now - tempsInici) + " en acabar d'executar sense threads");
        }

        public void Run()
        {
            contador = 0;
            Random r = new Random();
            DateTime tempsInici = DateTime.Now;
            Thread t = new Thread(() => CalculaPi(r, 0, nPunts / 2));
            Thread t2 = new Thread(() => CalculaPi(r, nPunts / 2, nPunts));
            t.Start();
            t2.Start();
            t.Join();
            t2.Join();
            double resultat = 4.0 * (contador / (double)nPunts);
            Console.WriteLine("Resultat " + resultat);
            Console.WriteLine("Ha tardat " + (DateTime.Now - tempsInici) + " en acabar d'executar amb threads");
        }

       

        private void CalculaPi(Random r, int inici, int final)
        {
            for (int i = inici; i < final; i++)
            {

                double coordenadax = r.NextDouble();
                double coordenaday = r.NextDouble();
                if (esValid(coordenadax, coordenaday, 0, 0) <= 1)
                {
                    lock (locker) {
                        contador++;
                    }
                }
            }

        }

        static double esValid(double x1, double y1, double x2, double y2)
        {
            double dX = x2 - x1;
            double dY = y2 - y1;
            return Math.Sqrt(dX * dX + dY * dY);
        }



    }
}
