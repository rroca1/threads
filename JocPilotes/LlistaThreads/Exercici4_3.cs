﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;

namespace LlistaThreads
{
    public class Exercici4_3 : Exercici
    {
        int[] vector;
        Random r;
        const int TAMANY_VECTOR = 100000;
        int suma;
        private readonly object locker = new object();

        public Exercici4_3()
        {
            r = new Random();
            suma = 0;
            vector = new int[TAMANY_VECTOR];
            for (int i = 0; i < vector.Length; i++)
            {
                vector[i] = r.Next(100);
            }
        }
        public void RunSenseThreads()
        {
            suma = 0;
            DateTime tempsInici = DateTime.Now;
            SumarNumeros(0, vector.Length);
            Console.WriteLine(suma);
            Console.WriteLine("Ha tardat " + (DateTime.Now - tempsInici) + " en acabar d'executar sense threads");
        }
        public void Run()
        {
            suma = 0;
            DateTime tempsInici = DateTime.Now;
            Thread t1 = new Thread(() => SumarNumeros(0, vector.Length / 2));
            Thread t2 = new Thread(() => SumarNumeros(vector.Length/2, vector.Length));
            t1.Start();
            t2.Start();
            t1.Join();
            t2.Join();
            Console.WriteLine(suma);
            Console.WriteLine("Ha tardat " + (DateTime.Now - tempsInici) + " en acabar d'executar amb threads");
        }

        private void SumarNumeros(int inici, int final)
        {
            for (int i = inici; i < final; i++)
            {
                lock(locker)
                {
                    suma += vector[i];
                }
            }
        }
    }
}
