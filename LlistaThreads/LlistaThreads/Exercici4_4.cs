﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;

namespace LlistaThreads
{
    public class Exercici4_4 : Exercici
    {
        int[,] taula1;
        int[,] taula2;
        int[,] resultat;

        public Exercici4_4()
        {
            this.taula1 = new int[,] {
              { 1, 2, 3 }, { 4, 5, 6 }, { 7, 8, 9 }
            };
            this.taula2 = new int[,] {
               { 1, 2, 3 }, { 4, 5, 6 }, { 7, 8, 9 }
            };
            this.resultat = new int[taula1.GetLength(0), taula2.GetLength(1)];

        }
        public void RunSenseThreads()
        {
            DateTime tempsInici = DateTime.Now;
            MultiplicaTaules(0, taula1.GetLength(0));
            Console.WriteLine("Ha tardat " + (DateTime.Now - tempsInici) + " en acabar d'executar sense threads");
        }

        public void Run()
        {
            DateTime tempsInici = DateTime.Now;
            Thread t = new Thread(() => MultiplicaTaules(0, taula1.GetLength(0) / 2));
            Thread t2 = new Thread(() => MultiplicaTaules(taula1.GetLength(0) / 2, taula1.GetLength(0)));
            t.Start();
            t2.Start();
            t.Join();
            t2.Join();
            Console.WriteLine("Ha tardat " + (DateTime.Now - tempsInici) + " en acabar d'executar amb threads");
        }

       

        private void MultiplicaTaules(int filaInici, int filaFinal)
        {
            for (int fila = filaInici; fila < filaFinal; fila++)
            {
                for (int columna = 0; columna < taula1.GetLength(1); columna++)
                {
                    resultat[0, columna] += taula1[fila, columna] * taula2[columna, fila];
                }
            }
        
        }


       
    }
}
