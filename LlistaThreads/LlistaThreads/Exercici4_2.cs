﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;

namespace LlistaThreads
{
    public class Exercici4_2 : Exercici
    {
        private readonly object locker = new object();
        private const int TEMPS = 1000;

        public Exercici4_2()
        {
           
        }
        public void RunSenseThreads()
        {
            DateTime tempsInici = DateTime.Now;
            ImprimirNumeroThread(1);
            ImprimirNumeroThread(2);
            ImprimirNumeroThread(3);
            ImprimirNumeroThread(4);
            ImprimirNumeroThread(5);
            ImprimirNumeroThread(6);
            Console.WriteLine("Ha tardat " + (DateTime.Now - tempsInici) + " en acabar d'executar sense threads");
        }

        public void Run()
        {
            DateTime tempsInici = DateTime.Now;
            Thread t1 = new Thread(() => ImprimirNumeroThread(1));
            Thread t2 = new Thread(() => ImprimirNumeroThread(2));
            Thread t3 = new Thread(() => ImprimirNumeroThread(3));
            Thread t4 = new Thread(() => ImprimirNumeroThread(4));
            Thread t5 = new Thread(() => ImprimirNumeroThread(5));
            Thread t6 = new Thread(() => ImprimirNumeroThread(6));

            t1.Start();
            t2.Start();
            t3.Start();
            t4.Start();
            t5.Start();
            t6.Start();

            t1.Join();
            t2.Join();
            t3.Join();
            t4.Join();
            t5.Join();
            t6.Join();
            Console.WriteLine("Ha tardat " + (DateTime.Now - tempsInici) + " en acabar d'executar amb threads");
        }

        private void ImprimirNumeroThread(int numero)
        {
            for (int i = 0; i < TEMPS; i++)
            {
                lock(locker)
                {
                    Thread.Sleep(1000);
                    Console.WriteLine(numero);
                }
               
            }
            
        }

        
    }
}
