﻿using System;

namespace LlistaThreads
{
    class Program
    {
        static void Main(string[] args)
        {
            /*
            Exercici4_1 exercici41 = new Exercici4_1();
            exercici41.RunSenseThreads();
            exercici41.Run();

            Exercici4_2 exercici42 = new Exercici4_2();
            exercici42.Run();

            Exercici4_3 exercici43 = new Exercici4_3();
            exercici43.RunSenseThreads();
            exercici43.Run();

            Exercici4_4 exercici44 = new Exercici4_4();
            exercici44.RunSenseThreads();
            exercici44.Run();
            */
            Exercici5_5 exercici55 = new Exercici5_5();
            exercici55.RunSenseThreads();
            exercici55.Run();

        }
    }
}
