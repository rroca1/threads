﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;

namespace LlistaThreads
{
    public class Exercici4_1 : Exercici
    {
      

        public Exercici4_1()
        {
           
        }
        public void RunSenseThreads()
        {
            DateTime tempsInici = DateTime.Now;
            ComptarFinsA100("A");
            ComptarFinsA100("B");
            ComptarFinsA100("C");
            Console.WriteLine("Ha tardat " + (DateTime.Now - tempsInici) + " en acabar d'executar sense threads");

        }

        public void Run()
        {
            DateTime tempsInici = DateTime.Now;
            Thread t1 = new Thread(() => ComptarFinsA100("A"));
            Thread t2 = new Thread(() => ComptarFinsA100("B"));
            Thread t3 = new Thread(() => ComptarFinsA100("C"));
            t1.Start();
            t2.Start();
            t3.Start();
            t1.Join();
            t2.Join();
            t3.Join();
            Console.WriteLine("Ha tardat " + (DateTime.Now - tempsInici) + " en acabar d'executar amb threads");
        }

        

        private void ComptarFinsA100(string lletraThread)
        {
            for (int i = 1; i <= 100; i++)
            {
                Console.WriteLine(lletraThread + i);
            }
        }

      

    }
}
